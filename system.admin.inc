<?php
/**
 * @file system.admin.inc
 */

/**
 * Configure liqpay module
 */ 
function liqpay_admin_settings() {
  $form['liqpay'] = array(
    '#type' => 'fieldset', 
    '#title' => t('LiqPay merchant settings'),
    '#description' => t('For more information, please visit !site', 
            array('!site' => l('http://liqpay.com/', 'http://liqpay.com/')))
  );
  $form['liqpay']['liqpay_merchant_id'] = array(
    '#type' => 'textfield', 
    '#title' => t('Merchant id'),
    '#description' => t('The identifier of merchant agent'),
    '#default_value' => variable_get('liqpay_merchant_id', NULL),
    '#required' => TRUE,
  );
  $form['liqpay']['liqpay_merchant_sign']  = array(
    '#type' => 'textfield', 
    '#title' => t('Merchant signature'),
    '#default_value' => variable_get('liqpay_merchant_sign', NULL),
    '#required' => TRUE,
  );
  $form['liqpay']['liqpay_result_url_dis']  = array(
    '#type' => 'textfield', 
    '#title' => t('Result URL'),
    '#default_value' => variable_get('liqpay_result_url', NULL),
    '#disabled' => TRUE,
  );
  $form['liqpay']['liqpay_server_url_dis']  = array(
    '#type' => 'textfield', 
    '#title' => t('Server URL'),
    '#default_value' => variable_get('liqpay_server_url', NULL),
    '#disabled' => TRUE,
  );
  $form['liqpay']['liqpay_result_url']  = array(
    '#type' => 'hidden', 
    '#default_value' => variable_get('liqpay_result_url', NULL),
    '#required' => TRUE,
  );
  $form['liqpay']['liqpay_server_url']  = array(
    '#type' => 'hidden', 
    '#default_value' => variable_get('liqpay_server_url', NULL),
    '#required' => TRUE,
  );
  $form['liqpay']['liqpay_default_currency']  = array(
    '#type' => 'select',
    '#options' => _liqpay_get_currency_list(), 
    '#title' => t('Default currency'),
    '#default_value' => variable_get('liqpay_default_currency', NULL),
    '#required' => TRUE,
  );
  $form['liqpay']['liqpay_description']  = array(
    '#type' => 'textfield',
    '#title' => t('Payment description'),
    '#default_value' => variable_get('liqpay_description', 'Default'),
    '#required' => TRUE,
  );
  $form['liqpay']['liqpay_order_prefix']  = array(
    '#type' => 'textfield',
    '#title' => t('Liqpay order prefix: example donate_'),
    '#default_value' => variable_get('liqpay_order_prefix', NULL),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}
